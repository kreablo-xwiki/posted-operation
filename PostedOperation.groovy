{{groovy output="none"}}

class PostedOperation {

    final String name

    final Closure function

    final String label

    final String parameterName

    final def script


    public PostedOperation(script, String name, Closure function, String label, String parameterName) {
        this.script = script
        this.name = name
        this.label = label
        this.function = function
	this.parameterName = parameterName
    }
  
    public PostedOperation(script, String name, Closure function, String label) {
	this(script, name, function, label, null)
    }

    public void outputCheckboxInput() {
        script.println('<dd><input type="checkbox" name="' + name + '" id="' + name + '" /><label for="' + name + '">' + label + '</label></dd>');
    }

    public void outputParameterInput() {
	if (this.parameterName != null) {
	    script.println('<dd><input type="text" name="' + this.parameterName + '" /><label for="' + this.parameterName + '"> Parameter till ' + label + '</label></dd>')
	}
    }

    public void checkAndCall(request) {
        if (request.get(name) == "on") {
            script.println('== ' + label + '==')
	    if (this.parameterName != null) {
		function(request.get(this.parameterName))
	    } else {
		function()
	    }
        }
    }

}

boolean maybeDoPostedOperations(request, PostedOperation [] postedOperations) {
    if (request.getMethod() == "POST" && services.csrf.getToken() == request.get('form_token')) {
        for (po in postedOperations) {
            po.checkAndCall(request)
        }
        return true
    }
    return false
}

void outputForm(PostedOperation [] postedOperations)
{
    println("((({{html}}")
    println('<form action="' + xwiki.getURL(doc.fullName) + '" method="POST" >')
    println('<input type="hidden" name="form_token" value="' + services.csrf.getToken() + '" />')
    println('<dl>')
    for (po in postedOperations) {
        po.outputCheckboxInput()
	po.outputParameterInput()
    }
    println('</dl>')
    println('<span class="buttonwrapper"><input type="submit" class="button" name="submit" value="Verkställ" /></span>');
    println('</form>')
    println("{{/html}})))")
}


{{/groovy}}
